<?php


namespace App\Components;


use Illuminate\Support\Facades\Storage;

class ImagesUtils
{

    public  function uploadImageBase64($baseURL, $nombreImagen, $img64)
    {
        if (!$img64) {
            return null;
        }

        $imageInfo = explode(";base64,", $img64);
        $imgExt = str_replace('data:image/', '', $imageInfo[0]);
        $image = str_replace(' ', '+', $imageInfo[1]);
        Storage::disk('s3')->put($baseURL .'/' . $nombreImagen, base64_decode($image), 'public');
        return Storage::disk('s3')->url($baseURL .'/' . $nombreImagen);
    }

}