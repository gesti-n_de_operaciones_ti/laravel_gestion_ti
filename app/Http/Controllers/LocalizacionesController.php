<?php

namespace App\Http\Controllers;

use App\Models\CIHardware;
use App\Models\Edificio;
use App\Models\ElementoConfiguracion;
use App\Models\Empleado;
use App\Models\Localizacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LocalizacionesController extends Controller
{

    public function index(Request $request){

        $idEdificio = $request->input('idEdificio', null);
        $orderBy = $request->input('orderBy', 'claveLocalizacion');

        $query = Localizacion::select(['localizaciones.*', 'edificios.clave as claveEdificio','edificios.descripcion as descEdificio'])
            ->join('edificios','edificios.idEdificio','=','localizaciones.idEdificio')
            ->orderBy($orderBy,'asc');

        if($idEdificio){
            $query->where('edificios.idEdificio', $idEdificio);
        }

        return response()->json($query->get(),200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $localizacion = Localizacion::create($data);

        return response()->json($localizacion, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $idLocalizacion
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($idLocalizacion)
    {
        $query = Localizacion::findOrFail($idLocalizacion);
        return response()->json($query,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $idLocalizacion)
    {
        $data = $request->all();
        $localizacion = Localizacion::findOrFail($idLocalizacion);
        $localizacion->update($data);

        return response()->json($localizacion,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($idLocalizacion)
    {
        return DB::transaction(function() use($idLocalizacion) {

            $numElementosLocalizacion= CIHardware::where('idLocalizacion','=',$idLocalizacion)->count();
            $localizacion = Localizacion::findOrFail($idLocalizacion);

            if($numElementosLocalizacion != 0){
                return response()->json(['No puede eliminar la localización debido a que existen elementos de configuración relacionados a esta.'],422);
            }

            $localizacion->delete();

            return response()->json($localizacion, 200);
        });
    }
}
