<?php

namespace App\Http\Controllers;

use App\Models\Proveedor;
use App\Models\Role;
use Illuminate\Http\Request;

class ProveedoresController extends Controller
{
    public function index(){
        $query = Proveedor::select('*');
        return response()->json($query->get(), 200);
    }
}
