<?php

namespace App\Http\Controllers;

use App\Models\Servicio;
use Illuminate\Http\Request;

class CatalogoServiciosController extends Controller
{
    public function index(Request $request){
        $orderBy = $request->input('orderBy', 'idProblema');

        $catalogo = Servicio::select('*')
            ->join('prioridades_solicitudes','prioridades_solicitudes.idPrioridad','=','problemas_conocidos.idPrioridad')
            ->orderBy($orderBy,'asc');

        return response()->json($catalogo->get(),200);
    }

    public function store(Request $request){
        $formData = $request->all();
        $servicio = Servicio::create($formData);

        return response()->json($servicio,200);
    }

    public function destroy(Request $request, $idServicio){
        $servicio = Servicio::findOrFail($idServicio);
        $servicio->delete();

        return response()->json($servicio,200);
    }

}
