<?php

namespace App\Http\Controllers;

use App\Models\CIDatabase;
use App\Models\CIHardware;
use App\Models\CINetwork;
use App\Models\CISoftware;
use App\Models\ElementoConfiguracion;
use App\Models\Role;
use App\Models\TipoCI;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ElementoConfiguracionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $siglasTipoCI= $request->input('siglasTipoCI', null);
        $idLocalizacion = $request->input('idLocalizacion', null);

        $columns = [
            'elemento_configuracion.idElementoConf',
            'elemento_configuracion.nbElemento',
            'descripcion',
            DB::raw('CONCAT(empleados.name," ",empleados.last_name) AS nbEncargado'),
            'nbTipoCI',
            'tipo_ci.siglasTipoCI',
            'proveedores.nbProveedor'
        ];

        $query = ElementoConfiguracion::select($columns)
            ->join('empleados','empleados.id','=','elemento_configuracion.idEncargado')
            ->leftJoin('proveedores','proveedores.idProveedor','=','elemento_configuracion.idProveedor')
            ->join('tipo_ci','tipo_ci.siglasTipoCI','=','elemento_configuracion.siglasTipoCI')
            ->orderBy('idElementoConf','asc');

        if($siglasTipoCI){
            $query->where('tipo_ci.siglasTipoCI',$siglasTipoCI);
        }

        if($idLocalizacion){
            $query->join('ci_hardware','ci_hardware.idElementoConf','elemento_configuracion.idElementoConf')
            ->where('idLocalizacion',$idLocalizacion);
        }

        return response()->json($query->get(),200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $formData = $request->all();

        return DB::transaction(function() use($formData){

//            $imagenCI =    $formData['image'] ;
            $siglasTipoCI = $formData['siglasTipoCI'] ;
            $dataTipoCI = $formData['data_ci'] ;

            $elemento_configuracion = ElementoConfiguracion::create($formData);

            switch ($siglasTipoCI){
                case 'HW':
                    $ci_hardware = new CIHardware($dataTipoCI);
                    $elemento_configuracion->hardware()->save($ci_hardware);
                    break;
                case 'SW':
                    $ci_software = new CISoftware($dataTipoCI);
                    $elemento_configuracion->software()->save($ci_software);
                    break;
                case 'DB':
                    $ci_database = new CIDatabase($dataTipoCI);
                    $elemento_configuracion->database()->save($ci_database);
                    break;
                case 'NET':
                    $ci_network = new CINetwork($dataTipoCI);
                    $elemento_configuracion->database()->save($ci_network);
                    break;
            }

            return response()->json($elemento_configuracion->get(), 200);
        });

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($idElementoConf)
    {
        $columns = [
            'elemento_configuracion.*',
            'descripcion',
            DB::raw('CONCAT(empleados.name," ",empleados.last_name) AS nbEncargado'),
            'nbTipoCI',
            'tipo_ci.siglasTipoCI',
            'proveedores.nbProveedor'
        ];

        $query = ElementoConfiguracion::select($columns)
            ->join('empleados','empleados.id','=','elemento_configuracion.idEncargado')
            ->leftJoin('proveedores','proveedores.idProveedor','=','elemento_configuracion.idProveedor')
            ->join('tipo_ci','tipo_ci.siglasTipoCI','=','elemento_configuracion.siglasTipoCI')
            ->where('elemento_configuracion.idElementoConf',$idElementoConf)->first();

        $dataCI = $query->dataCi;

        return response()->json($query,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $idElementoConf)
    {
        //

        $formData = $request->all();

        return DB::transaction(function() use($formData, $idElementoConf){

            $siglasTipoCI = $formData['siglasTipoCI'] ;
            $dataTipoCI = $formData['data_ci'] ;

            $elemento_configuracion = ElementoConfiguracion::findOrFail($idElementoConf);

            $elemento_configuracion->update($formData);

            switch ($siglasTipoCI){
                case 'HW':
                    $ci_hardware = CIHardware::findOrFail($idElementoConf);
                    $ci_hardware->update($dataTipoCI);
                    break;
                case 'SW':
                    $ci_software = CISoftware::findOrFail($idElementoConf);
                    $ci_software->update($dataTipoCI);
                    break;
                case 'DB':
                    $ci_database = CIDatabase::findOrFail($idElementoConf);
                    $ci_database->update($dataTipoCI);
                    break;
                case 'NET':
                    $ci_network = CINetwork::findOrFail($idElementoConf);
                    $ci_network->update($dataTipoCI);
                    break;
            }

            return response()->json($elemento_configuracion->get(), 200);
        });

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($idElementoConf)
    {
        $elemento_configuracion = ElementoConfiguracion::findOrFail($idElementoConf);
        $elemento_configuracion->delete();
        return response()->json($elemento_configuracion);
    }

    public function tiposCI(Request $request)
    {
        $query = TipoCI::select('*');
        return response()->json($query->get(), 200);
    }
}
