<?php

namespace App\Http\Controllers;

use App\Models\Solicitud;
use App\Models\SolicitudCambio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CambiosSolicitudesController extends Controller
{
    public function index(Request $request){
        $idElementoConf = $request->input('idElementoConf', null);

        $columns = [
            'solicitud_cambio.*',
            'estatus_cambio.*',
            'ci.nbElemento',
            'ci.descripcion as descripcionCI',
            DB::raw('CONCAT(empleados.name," ",empleados.last_name) AS nbEncargado'),
            'proveedores.*',
            'nbTipoCI',
            'tipo_ci.siglasTipoCI',
        ];

        $query = SolicitudCambio::select($columns)
            ->join('elemento_configuracion as ci','ci.idElementoConf','=','solicitud_cambio.idElementoConf')
            ->join('estatus_cambio','estatus_cambio.idEstatusCambio','=','solicitud_cambio.idEstatusCambio')
            ->join('empleados','empleados.id','=','ci.idEncargado')
            ->leftJoin('proveedores','proveedores.idProveedor','=','ci.idProveedor')
            ->join('tipo_ci','tipo_ci.siglasTipoCI','=','ci.siglasTipoCI')
            ->orderBy('idSolicitudCambio','asc');

        return response()->json($query->get(),200);
    }

    public function show(Request $request,$idSolicitudCambio){

        $columns = [
            'solicitud_cambio.*',
            'estatus_cambio.*',
            'ci.nbElemento',
            'ci.descripcion as descripcionCI',
            DB::raw('CONCAT(empleados.name," ",empleados.last_name) AS nbEncargado'),
            'proveedores.*',
            'nbTipoCI',
            'tipo_ci.siglasTipoCI',
        ];

        $query = SolicitudCambio::select($columns)
            ->join('elemento_configuracion as ci','ci.idElementoConf','=','solicitud_cambio.idElementoConf')
            ->join('estatus_cambio','estatus_cambio.idEstatusCambio','=','solicitud_cambio.idEstatusCambio')
            ->join('empleados','empleados.id','=','ci.idEncargado')
            ->leftJoin('proveedores','proveedores.idProveedor','=','ci.idProveedor')
            ->join('tipo_ci','tipo_ci.siglasTipoCI','=','ci.siglasTipoCI')
            ->where('solicitud_cambio.idSolicitudCambio',$idSolicitudCambio)
            ->orderBy('idSolicitudCambio','asc')
            ->first();

        return response()->json($query,200);
    }

    public function approve(Request $request, $idSolicitudCambio){

        return DB::transaction(function() use($idSolicitudCambio){
            $solicitudCambio = SolicitudCambio::findOrFail($idSolicitudCambio);
            $incidencia = Solicitud::find($solicitudCambio->idIncidencia);

            $solicitudCambio->idEstatusCambio = SolicitudCambio::ESTATUS_APROBADO;
            $solicitudCambio->save();

            $incidencia->idEstatus = Solicitud::ESTATUS_EN_PROCESO;
            $incidencia->save();

            return response()->json($solicitudCambio,200);
        });
    }

    public function deny(Request $request, $idSolicitudCambio){

        \Log::info('ENTRA DENY');

        return DB::transaction(function() use($idSolicitudCambio){
            $solicitudCambio = SolicitudCambio::findOrFail($idSolicitudCambio);
            $incidencia = Solicitud::find($solicitudCambio->idIncidencia);

            $solicitudCambio->idEstatusCambio = SolicitudCambio::ESTATUS_RECHAZADO;
            $solicitudCambio->save();

            $incidencia->idEstatus = Solicitud::ESTATUS_CANCELADA;
            $incidencia->fhTerminacion = date("Y-m-d H:i:s");
            $incidencia->save();

            return response()->json($solicitudCambio,200);
        });
    }
}
