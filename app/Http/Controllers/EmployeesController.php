<?php

namespace App\Http\Controllers;

use App\Models\Departamentos;
use App\Models\Empleado;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $perPage = $request->input('perPage', 10);
        $roles = $request->input('roles', null);
        $idDepartamento = $request->input('idDepartamento', null);

        $columns=[
            'empleados.id',
            'empleados.email',
            'empleados.name',
            'empleados.last_name',
            'empleados.username',
            'empleados.address',
            'empleados.phone',
            'empleados.idRol',
            'roles.nbRol'
        ];

        $query = Empleado::with('departamento')
            ->select($columns)
            ->join('roles','empleados.idRol','=','roles.idRol');

        if($roles){
            $rolesEmp = explode(',', $roles);
            $query->whereIn('roles.idRol', $rolesEmp);
        }

        if($idDepartamento){
            $query->where('empleados.idDepartamento', $idDepartamento);
        }

        return response()->json($query->paginate($perPage), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $password = $request->input('default_password');
        $idDepartamento = $request->input('idDepartamento',null);

        $usernameEmp = Empleado::where('username','=',$data['username'])
                                        ->orWhere('email','=',$data['email'])->first();

        if($usernameEmp){
            return response()->json(['msg' => 'El nombre de usuario o correo electrónico ingresado no está disponible.'],422);
        }

        $data['password'] = Hash::make($password);

        $empleado = DB::transaction(function () use($data, $idDepartamento) {
            $empleado = Empleado::create($data);

            DB::table('departamentos_empleados')->insert([
                'idDepartamento' => $idDepartamento,
                'idEmpleado'=>$empleado->id
            ]);
        });

        return response()->json($empleado, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($idEmpleado)
    {
            $query = Empleado::with('departamento')->findOrFail($idEmpleado);
            $query->idDepartamento = $query->departamento[0]['idDepartamento'];
            return response()->json($query,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $idEmpleado)
    {
        $formData = $request->all();

        return DB::transaction(function() use($formData,$idEmpleado){
            $empleado = Empleado::with('departamento')->findOrFail($idEmpleado);
            $empleado->update($formData);

            if($formData['idDepartamento']){
                $departamento = DB::table('departamentos_empleados')
                    ->where('idEmpleado',$idEmpleado);

                $departamento->update([
                    'idDepartamento' => $formData['idDepartamento']
                ]);
            }
            return response()->json($empleado, 200);
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($idEmpleado)
    {
        return DB::transaction(function() use($idEmpleado) {
            $departamento = DB::table('departamentos_empleados')
                ->where('idEmpleado',$idEmpleado);

            $departamento->delete();

            $empleado = Empleado::findOrFail($idEmpleado);
            $empleado->delete();
            return response()->json($empleado, 200);
        });
    }


    public function rolesUser(Request $request)
    {
        $query = Role::select('*');
        return response()->json($query->get(), 200);
    }

}
