<?php

namespace App\Http\Controllers;

use App\Models\CuestionarioProducto;
use App\Models\ElementoConfiguracion;
use App\Models\Empleado;
use App\Models\EvaluacionSolicitud;
use App\Models\HistorialSolicitud;
use App\Models\Prioridad;
use App\Models\Quiz;
use App\Models\Role;
use App\Models\Solicitud;
use App\Models\SolicitudCambio;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class IncidenciasController extends Controller
{

    public function index(Request $request){
        $orderBy = $request->input('orderBy', 'idSolicitud');
        $idSolicitante = $request->input('idSolicitante', null);
        $idTecnico = $request->input('idTecnico', null);

        $columns = [
            'idSolicitud',
            'solicitudes.descripcion',
            'solicitudes.idElementoConf',
            'elemento_configuracion.nbElemento',
            'fhInicio',
            'fhTerminacion',
            'tiempoEstimado',
            'solicitudes.idPrioridad',
            'solicitudes.idEstatus',
            'estatus_solicitudes.nbEstatus',
        ];

        $query = Solicitud::select($columns)
            ->join('elemento_configuracion','elemento_configuracion.idElementoConf','=','solicitudes.idElementoConf')
            ->join('estatus_solicitudes','estatus_solicitudes.idEstatus','solicitudes.idEstatus')
            ->orderBy($orderBy,'asc');

        if($idSolicitante){
            $query->where('idSolicitante',$idSolicitante);
        }

        if($idTecnico){
            $query->where('idTecnico',$idTecnico);
        }

        return response()->json($query->get(),200);
    }

    public function create(Request $request){
        $formData = $request->all();
        $user = Auth::user();

        return DB::transaction(function() use($formData,$user){
            $solicitud = Solicitud::create([
                'descripcion' => $formData['descripcion'],
                'idElementoConf' => $formData['idElementoConf'],
                'idSolicitante' => $user->id,
                'idEstatus' => Solicitud::ESTATUS_ENVIADA,
                'fhInicio' => date("Y-m-d H:i:s")
            ]);

            $historialSolicitud =HistorialSolicitud::create([
                'idEstatus' => Solicitud::ESTATUS_ENVIADA,
                'fhCambio' => date("Y-m-d H:i:s"),
                'idSolicitud' => $solicitud->idSolicitud
            ]);

            return response()->json($solicitud,200);
        });
    }

    public function show(Request $request, $idSolicitud){
        $solicitud = Solicitud::select(['solicitudes.*','estatus_solicitudes.nbEstatus','prioridades_solicitudes.nbPrioridad'])
            ->leftJoin('prioridades_solicitudes','prioridades_solicitudes.idPrioridad','=','solicitudes.idPrioridad')
            ->join('estatus_solicitudes','estatus_solicitudes.idEstatus','=','solicitudes.idEstatus')
            ->where('idSolicitud',$idSolicitud)->first();

        $elementoConfiguracion = ElementoConfiguracion::where('idElementoConf',$solicitud->idElementoConf)->first();

        $empleadoSolictante = Empleado::where('id',$solicitud->idSolicitante)->first();
        $empleadoTecnico = Empleado::where('id',$solicitud->idTecnico)->first();

        $solicitud->elementoConfiguracion = $elementoConfiguracion;

        $solicitud->solicitante = $empleadoSolictante;
        $solicitud->tecnico = $empleadoTecnico;

        return response()->json($solicitud,200);
    }

    public function asignarSolicitud(Request $request, $idSolicitud){

        $formData = $request->all();

        return DB::transaction(function() use($formData,$idSolicitud){

            $solicitud = Solicitud::findOrFail($idSolicitud);

            $solicitud->idPrioridad = $formData['idPrioridad'];
            $solicitud->idTecnico = $formData['idTecnico'];
            $solicitud->idEstatus = Solicitud::ESTATUS_EN_DIAGNOSTICO;
            $solicitud->save();

            $historialSolicitud =HistorialSolicitud::create([
                'idEstatus' => Solicitud::ESTATUS_EN_DIAGNOSTICO,
                'fhCambio' => date("Y-m-d H:i:s"),
                'idSolicitud' => $solicitud->idSolicitud
            ]);

            return response()->json($solicitud,200);
        });

    }

    public function diagnosticarSolicitud(Request $request, $idSolicitud){

        $formData = $request->all();

        return DB::transaction(function() use($formData,$idSolicitud){

            $solicitud = Solicitud::findOrFail($idSolicitud);
            $solicitud->tiempoEstimado = $formData['tiempoEstimado'];
            $solicitud->descripcionServicio = $formData['descripcionServicio'];
            $solicitud->idEstatus = Solicitud::ESTATUS_EN_PROCESO;

            if(isset($formData['descripcionCambio'])){
                $solicitudCambio = SolicitudCambio::create([
                    'descripcion' => $formData['descripcionCambio'],
                    'idIncidencia' => $solicitud->idSolicitud,
                    'idElementoConf' => $solicitud->idElementoConf,
                    'idEstatusCambio' => 1
                ]);

                $solicitud->idEstatus = Solicitud::ESTATUS_EN_ESPERA_CAMBIO;

                $historialSolicitud =HistorialSolicitud::create([
                    'idEstatus' => Solicitud::ESTATUS_EN_PROCESO,
                    'fhCambio' => date("Y-m-d H:i:s"),
                    'idSolicitud' => $solicitud->idSolicitud
                ]);
            }

            $solicitud->save();

            return response()->json($solicitud,200);
        });
    }

    public function procesarSolicitud(Request $request, $idSolicitud){

        $formData = $request->all();

        return DB::transaction(function() use($formData,$idSolicitud){

            $solicitud = Solicitud::findOrFail($idSolicitud);

            $solicitud->descripcionSolucion = $formData['descripcionSolucion'];
            $solicitud->idEstatus = Solicitud::ESTATUS_PROCESADA;
            $solicitud->save();

            $historialSolicitud =HistorialSolicitud::create([
                'idEstatus' => Solicitud::ESTATUS_PROCESADA,
                'fhCambio' => date("Y-m-d H:i:s"),
                'idSolicitud' => $solicitud->idSolicitud
            ]);

            return response()->json($solicitud,200);
        });
    }

    public function evaluarSolicitud(Request $request, $idSolicitud){

        $formData = $request->all();

        return DB::transaction(function() use($formData,$idSolicitud){

            $solicitud = Solicitud::findOrFail($idSolicitud);
            $solicitud->idEstatus = Solicitud::ESTATUS_APROBADA;
            $solicitud->fhTerminacion = date("Y-m-d H:i:s");
            $solicitud->save();

            $historialSolicitud =HistorialSolicitud::create([
                'idEstatus' => Solicitud::ESTATUS_APROBADA,
                'fhCambio' => date("Y-m-d H:i:s"),
                'idSolicitud' => $solicitud->idSolicitud
            ]);

            $evaluacionSolicitud = EvaluacionSolicitud::create(
                [
                    'resena' => $formData['resena'],
                    'questionario_data' => $formData['quizData'],
                    'calificacion' => $formData['calificacion'],
                    'idSolicitud' => $idSolicitud
                ]
            );

            return response()->json($solicitud,200);
        });
    }

    public function prioridadesSolicitudes(Request $request)
    {
        $query = Prioridad::select('*');
        return response()->json($query->get(), 200);
    }

    public function historialSolicitud(Request $request, $idSolicitud){
        $columns = [
            'historial_solicitudes.*',
            'estatus_solicitudes.nbEstatus'
        ];

        $historialSolicitud = HistorialSolicitud::select($columns)
            ->join('estatus_solicitudes','estatus_solicitudes.idEstatus','=','historial_solicitudes.idEstatus')
            ->where('idSolicitud',$idSolicitud)->get();

        return response()->json($historialSolicitud, 200);
    }
}
