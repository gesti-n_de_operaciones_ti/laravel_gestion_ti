<?php

namespace App\Http\Controllers;

use App\Models\Edificio;
use App\Models\ElementoConfiguracion;
use App\Models\Localizacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EdificiosController extends Controller
{
    public function index(Request $request){

        $edificios = Edificio::select('*')
            ->orderBy('clave','asc')->get();

        return response()->json($edificios,200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $edificio = Edificio::create($data);

        return response()->json($edificio, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $idEdificio
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($idEdificio)
    {
        $query = Edificio::findOrFail($idEdificio);
        return response()->json($query,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $idEdificio
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $idEdificio)
    {
        $data = $request->all();
        $edificio = Edificio::findOrFail($idEdificio);
        $edificio->update($data);

        return response()->json($edificio,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($idEdificio)
    {
        return DB::transaction(function() use($idEdificio) {

            $numLocalizaciones= Localizacion::where('idEdificio','=',$idEdificio)->count();
            $edificio = Edificio::findOrFail($idEdificio);

            if($numLocalizaciones != 0){
                return response()->json(['No puede eliminar un edificio que está relacionado con localizaciones activas.'],422);
            }

            $edificio->delete();
            return response()->json($edificio, 200);
        });
    }

}
