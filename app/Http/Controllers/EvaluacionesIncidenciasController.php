<?php

namespace App\Http\Controllers;

use App\Models\EvaluacionSolicitud;
use App\Models\Quiz;
use Illuminate\Http\Request;

class EvaluacionesIncidenciasController extends Controller
{
    public function getQuizEvaluacion() {
        $quizEvaluacion = Quiz::with('questions.options')
            ->find(1);

        return response()->json($quizEvaluacion, 200);
    }

    public function evaluacionSolicitud(Request $request, $idSolicitud) {
        $evaluacion = EvaluacionSolicitud::where('idSolicitud',$idSolicitud) ->first();

        if(isset($evaluacion)){
            $evaluacion['questionario_data'] = json_decode($evaluacion['questionario_data']);
        }

        $evaluacion['quiz'] = Quiz::with('questions.options')
            ->find(1);

        return response()->json($evaluacion, 200);
    }
}
