<?php

namespace App\Http\Controllers;

use App\Models\DepartamentoEncargado;
use App\Models\Departamentos;
use App\Models\Empleado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartamentosController extends Controller
{
    public function index(Request $request){
        $orderBy = $request->input('orderBy', 'nbDepartamento');

        $departments = Departamentos::select('*')
            ->with('encargado')
            ->orderBy($orderBy,'asc');

        return response()->json($departments->get(),200);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        return DB::transaction(function () use($data){
            $departamento = Departamentos::create($data);
            $idEncargado = $data['idEncargado'];

            if($idEncargado){
                DB::table('departamento_encargado')->insert([
                    'idEncargado' => $idEncargado,
                    'idDepartamento' => $departamento->idDepartamento
                ]);
            }

            return response()->json($departamento, 200);
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $idDepartamento
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($idDepartamento)
    {
        $query = Departamentos::with('encargado')->findOrFail($idDepartamento);
        return response()->json($query,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $idDepartamento
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $idDepartamento)
    {
        $data = $request->all();
        return DB::transaction(function () use($data, $idDepartamento){
            $departamento = Departamentos::findOrFail($idDepartamento);
            $departamento->update($data);

            $idEncargado = $data['idEncargado'];

            if($idEncargado){
                DB::table('departamento_encargado')->updateOrInsert([
                    'idEncargado' => $idEncargado,
                    'idDepartamento' => $departamento->idDepartamento
                ]);
            }

            return response()->json($departamento, 200);
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($idDepartamento)
    {
        return DB::transaction(function() use($idDepartamento) {

            $numEmpleadosDepa= DB::table('departamentos_empleados')
                ->where('idDepartamento','=',$idDepartamento)->count();
            $departamento = Departamentos::findOrFail($idDepartamento);

            if($numEmpleadosDepa != 0){
                return response()->json(['No puede eliminar un departamento que tiene empleados asignados.'],422);
            }

            DB::table('departamento_encargado')
                ->where('idDepartamento','=',$idDepartamento)
                ->delete();
            $departamento->delete();

            return response()->json($departamento, 200);
        });
    }

}
