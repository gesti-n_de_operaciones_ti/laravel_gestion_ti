<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EvaluacionSolicitud extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'evaluacion_solicitud';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'idEvaluacion';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'questionario_data',
        'idSolicitud',
        'calificacion',
        'resena'
    ];
}
