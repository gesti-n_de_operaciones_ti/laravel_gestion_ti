<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ElementoConfiguracion extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'elemento_configuracion';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'idElementoConf';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
//    protected $with = ['dataCI'];

    public function hardware()
    {
        return $this->hasOne(CIHardware::class,'idElementoConf','idElementoConf');
    }

    public function software()
    {
        return $this->hasOne(CISoftware::class,'idElementoConf','idElementoConf');
    }

    public function database()
    {
        return $this->hasOne(CIDatabase::class,'idElementoConf','idElementoConf');
    }

    public function network()
    {
        return $this->hasOne(CINetwork::class,'idElementoConf','idElementoConf');
    }

    public function dataCi(){
        $siglasTipoCI =$this->getAttributeValue('siglasTipoCI');

        switch ($siglasTipoCI) {
            case 'HW':
                $columnsHW = [
                    'ci_hardware.*',
                    'localizaciones.*',
                    'edificios.clave as claveEdificio',
                    'edificios.descripcion as descripcionEdificio',
                    'edificios.descUbicacion as ubicacionEdificio'
                ];

                return $this->hardware()->select($columnsHW)
                    ->join('localizaciones','ci_hardware.idLocalizacion','localizaciones.idLocalizacion')
                    ->join('edificios','localizaciones.idEdificio','edificios.idEdificio');
                break;
            case 'SW':
                return $this->software();
                break;
            case 'DB':
                return $this->database();
                break;
            case 'NET':
                return $this->network()->select(['ci_network.*','departamentos.*'])
                    ->join('departamentos','departamentos.idDepartamento','ci_network.idDepartamento');
                break;
        }

        return 'data';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idElementoConf',
        'nbElemento',
        'descripcion',
        'siglasTipoCI',
        'fhAdquisicion',
        'idEncargado',
        'idProveedor',
        'urlImage'
    ];
}
