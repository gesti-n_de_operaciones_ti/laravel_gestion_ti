<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SolicitudCambio extends Model
{
    use HasFactory;

    const ESTATUS_SOLICITADO = 1;
    const ESTATUS_APROBADO = 2;
    const ESTATUS_RECHAZADO= 3;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'solicitud_cambio';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'idSolicitudCambio';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idSolicitudCambio',
        'idIncidencia',
        'idElementoConf',
        'idEstatusCambio',
        'descripcion'
    ];
}
