<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Solicitud extends Model
{
    use HasFactory;

    const ESTATUS_ENVIADA = 1;
    const ESTATUS_EN_DIAGNOSTICO= 2;
    const ESTATUS_EN_PROCESO = 3;
    const ESTATUS_EN_ESPERA_CAMBIO = 4;
    const ESTATUS_PROCESADA = 5;
    const ESTATUS_APROBADA = 6;
    const ESTATUS_CANCELADA = 7;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'solicitudes';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'idSolicitud';

    public function itemCi()
    {
        return $this->belongsTo(ElementoConfiguracion::class,'idElementoConf','idElementoConf');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idSolicitud',
        'descripcion',
        'descripcionServicio',
        'descripcionSolucion',
        'fhInicio',
        'fhTerminacion',
        'idPrioridad',
        'rfc',
        'idElementoConf',
        'idTecnico',
        'idSolicitante',
        'idEstatus'
    ];
}
