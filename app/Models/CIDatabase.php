<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CIDatabase extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ci_database';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'idElementoConf';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idElementoConf',
        'ip_address',
        'dbms_name',
        'version'
    ];
}
