<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departamentos extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'departamentos';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'idDepartamento';

    public $timestamps = false;

    /**
     * Obtener el empleado encargado del departamento.
     *
     * @return mixed
     */
    public function encargado(){
        return $this->hasOneThrough(Empleado::class,DepartamentoEncargado::class,
            'idDepartamento',
            'id',
            'idDepartamento',
            'idEncargado'
        );
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idDepartamento',
        'nbDepartamento'
    ];
}
