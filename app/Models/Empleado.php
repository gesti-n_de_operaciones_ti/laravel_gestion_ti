<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Empleado extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

      /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'empleados';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'idRol',
        'phone',
        'address',
        'last_name',
        'username',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ 'password','remember_token'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Obtener los roles asignados para el usuario.
     *
     * @return mixed
     */
    public function roles(){
        return $this->belongsTo(Role::class,'idEmpleado','idRol');
    }

    /**
     * Obtener los roles asignados para el usuario.
     *
     * @return mixed
     */
    public function departamento(){
        return $this->belongsToMany(Departamentos::class,'departamentos_empleados','idEmpleado','idDepartamento');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {

        return [
            'user' => [
                'id'                  => $this->id,
                'name'           => $this->name,
                'last_name'   => $this->last_name,
                'role'              => $this->idRol,
                'email'           => $this->email,
            ]
        ];
    }
}
