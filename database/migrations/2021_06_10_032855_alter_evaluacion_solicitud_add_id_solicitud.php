<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterEvaluacionSolicitudAddIdSolicitud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluacion_solicitud', function (Blueprint $table) {
            $table->integer('idSolicitud')->after('idEvaluacion')->unsigned();

            $table->foreign('idSolicitud')
                ->references('idSolicitud')
                ->on('solicitudes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluacion_solicitud', function (Blueprint $table) {
            $table->dropForeign(['idSolicitud']);
            $table->dropColumn('idSolicitud');
        });
    }
}
