<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartamentoEncargado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departamento_encargado', function (Blueprint $table) {
            $table->integer('idDepartamento')->unsigned();
            $table->bigInteger('idEncargado')->unsigned();
            $table->timestamps();

            $table->foreign('idDepartamento')
                ->references('idDepartamento')
                ->on('departamentos');

            $table->foreign('idEncargado')
                ->references('id')
                ->on('empleados');

            $table->unique(['idEncargado', 'idDepartamento']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departamento_encargado');
    }
}
