<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCiSoftware extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ci_software', function (Blueprint $table) {
            $table->integer('idElementoConf')->primary()->unsigned();
            $table->string('ip_address');
            $table->string('version');
            $table->timestamps();

            $table->foreign('idElementoConf')
                ->references('idElementoConf')
                ->on('elemento_configuracion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ci_software');
    }
}
