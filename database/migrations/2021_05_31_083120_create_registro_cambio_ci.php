<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistroCambioCi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_cambio_ci', function (Blueprint $table) {
            $table->increments('idCambio');
            $table->integer('idSolicitud')->unsigned();
            $table->string('descripcion');
            $table->timestamps();

            $table->foreign('idSolicitud')
                ->references('idSolicitud')
                ->on('solicitudes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_cambio_ci');
    }
}
