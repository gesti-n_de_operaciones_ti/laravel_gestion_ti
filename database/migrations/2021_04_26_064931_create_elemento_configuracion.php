<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElementoConfiguracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elemento_configuracion', function (Blueprint $table) {
            $table->increments('idElementoConf');
            $table->string('nbElemento');
            $table->string('descripcion');
            $table->string('siglasTipoCI');
            $table->bigInteger('idEncargado')->unsigned();
            $table->integer('idProveedor')->unsigned()->nullable();

            $table->foreign('siglasTipoCI')
                ->references('siglasTipoCI')
                ->on('tipo_ci');

            $table->foreign('idEncargado')
                ->references('id')
                ->on('empleados');

            $table->foreign('idProveedor')
                ->references('idProveedor')
                ->on('proveedores');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elemento_configuracion');
    }
}
