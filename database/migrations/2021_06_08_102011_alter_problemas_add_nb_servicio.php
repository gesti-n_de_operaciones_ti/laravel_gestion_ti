<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProblemasAddNbServicio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('problemas_conocidos', function (Blueprint $table) {
            $table->string('nbServicio')->after('idProblema');
            $table->float('tiempoEstimado')->after('solucion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('problemas_conocidos', function (Blueprint $table) {
            $table->dropColumn('nbServicio');
            $table->dropColumn('tiempoEstimado');
        });
    }
}
