<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalizacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('localizaciones', function (Blueprint $table) {
            $table->increments('idLocalizacion');
            $table->char('claveLocalizacion');
            $table->string('descUbicacion')->nullable();
            $table->integer('idEdificio')->unsigned();
            $table->integer('numPlanta');

            $table->foreign('idEdificio')
                ->references('idEdificio')
                ->on('edificios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('localizaciones');
    }
}
