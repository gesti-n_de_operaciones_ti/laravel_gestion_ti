<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->increments('idSolicitud');
            $table->string('descripcion');
            $table->date('fhInicio');
            $table->date('fhTerminacion');
            $table->integer('idPrioridad')->unsigned()->nullable();
            $table->string('rfc')->nullable();
            $table->integer('idElementoConf')->unsigned()->nullable();
            $table->bigInteger('idTecnico')->unsigned()->nullable();
            $table->bigInteger('idSolicitante')->unsigned();
            $table->integer('idEstatus')->unsigned();
            $table->timestamps();

            $table->foreign('idTecnico')
                ->references('id')
                ->on('empleados');

            $table->foreign('idSolicitante')
                ->references('id')
                ->on('empleados');

            $table->foreign('idPrioridad')
                ->references('idPrioridad')
                ->on('prioridades_solicitudes');

            $table->foreign('idElementoConf')
                ->references('idElementoConf')
                ->on('elemento_configuracion');

            $table->foreign('idEstatus')
                ->references('idEstatus')
                ->on('estatus_solicitudes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes');
    }
}
