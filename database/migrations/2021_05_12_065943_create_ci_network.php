<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCiNetwork extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ci_network', function (Blueprint $table) {
            $table->integer('idElementoConf')->unsigned()->primary();
            $table->string('network_address');
            $table->string('subnet_mask');
            $table->string('nbNetwork');
            $table->integer('idDepartamento')->unsigned();
            $table->timestamps();

            $table->foreign('idElementoConf')
                ->references('idElementoConf')
                ->on('elemento_configuracion');

            $table->foreign('idDepartamento')
                ->references('idDepartamento')
                ->on('departamentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ci_network');
    }
}
