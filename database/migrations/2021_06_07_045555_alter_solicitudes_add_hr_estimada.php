<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSolicitudesAddHrEstimada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->string('tiempoEstimado')->nullable()->after('fhTerminacion');
            $table->dateTime('fhInicio')->nullable()->change();
            $table->dateTime('fhTerminacion')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->dropColumn('tiempoEstimado');
            $table->date('fhInicio')->change();
            $table->date('fhTerminacion')->change();
        });
    }
}

