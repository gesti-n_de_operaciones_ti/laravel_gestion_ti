<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterElementoConfiguracionAddFhAdquisicion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('elemento_configuracion', function (Blueprint $table) {
            $table->date('fhAdquisicion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('elemento_configuracion', function (Blueprint $table) {
            $table->dropColumn('fhAdquisicion');
        });
    }
}
