<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCiHardware extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ci_hardware', function (Blueprint $table) {
            $table->integer('idElementoConf')->unsigned()->primary();
            $table->string('numSerie');
            $table->string('modelo');
            $table->string('ip_address')->nullable();
            $table->integer('idLocalizacion')->unsigned();
            $table->timestamps();

            $table->foreign('idElementoConf')
                ->references('idElementoConf')
                ->on('elemento_configuracion');

            $table->foreign('idLocalizacion')
                ->references('idLocalizacion')
                ->on('localizaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ci_hardware');
    }
}
