<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('questions_groups', function (Blueprint $table) {
            $table->increments('idQuiz');
            $table->string('nbQuiz');
            $table->string('deQuiz')->nullable();

        });

        Schema::create('questions', function (Blueprint $table) {
            $table->increments('idQuestion');
            $table->integer('idQuiz')->unsigned();
            $table->string('value')->nullable();
            $table->string('key');
            $table->string('label');
            $table->boolean('required');
            $table->smallInteger('order');
            $table->string('controlType');
            $table->string('type')->nullable();

            $table->foreign('idQuiz')
                ->references('idQuiz')
                ->on('questions_groups');
        });

        Schema::create('question_options', function (Blueprint $table) {
            $table->increments('idOption');
            $table->integer('idQuestion')->unsigned();
            $table->string('key');
            $table->string('value');

            $table->foreign('idQuestion')
                ->references('idQuestion')
                ->on('questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_options');
        Schema::dropIfExists('questions');
        Schema::dropIfExists('questions_groups');
    }

}
