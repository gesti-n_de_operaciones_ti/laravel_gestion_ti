<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudCambio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_cambio', function (Blueprint $table) {
            $table->increments('idSolicitudCambio');
            $table->integer('idIncidencia')->unsigned();
            $table->integer('idElementoConf')->unsigned();
            $table->integer('idEstatusCambio')->unsigned();
            $table->string('descripcion');
            $table->timestamps();

            $table->foreign('idEstatusCambio')
                ->references('idEstatusCambio')
                ->on('estatus_cambio');

            $table->foreign('idIncidencia')
                ->references('idSolicitud')
                ->on('solicitudes');

            $table->foreign('idElementoConf')
                ->references('idElementoConf')
                ->on('elemento_configuracion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_cambio');
    }
}
