<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProblemasConocidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('problemas_conocidos', function (Blueprint $table) {
            $table->increments('idProblema');
            $table->string('descripcion');
            $table->string('solucion');
            $table->integer('idPrioridad')->unsigned();
            $table->timestamps();

            $table->foreign('idPrioridad')
                ->references('idPrioridad')
                ->on('prioridades_solicitudes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('problemas_conocidos');
    }
}
