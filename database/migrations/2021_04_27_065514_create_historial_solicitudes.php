<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialSolicitudes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_solicitudes', function (Blueprint $table) {
            $table->increments('idHistorial');
            $table->integer('idEstatus')->unsigned();
            $table->integer('idSolicitud')->unsigned();
            $table->date('fhCambio');

            $table->foreign('idEstatus')
                ->references('idEstatus')
                ->on('estatus_solicitudes');

            $table->foreign('idSolicitud')
                ->references('idSolicitud')
                ->on('solicitudes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_solicitudes');
    }
}
