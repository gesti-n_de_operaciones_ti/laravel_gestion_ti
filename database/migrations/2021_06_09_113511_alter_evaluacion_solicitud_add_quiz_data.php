<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterEvaluacionSolicitudAddQuizData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluacion_solicitud', function (Blueprint $table) {
            $table->string('questionario_data')->after('resena');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluacion_solicitud', function (Blueprint $table) {
            $table->dropColumn('questionario_data');
        });
    }
}
