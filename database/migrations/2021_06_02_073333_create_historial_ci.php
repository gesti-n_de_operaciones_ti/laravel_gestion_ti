<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialCi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_ci', function (Blueprint $table) {
            $table->id();
            $table->integer('idElementoConf')->unsigned();
            $table->string('nbElemento');
            $table->string('descripcion');
            $table->integer('idEncargado')->unsigned();
            $table->integer('idProveedor')->unsigned();
            $table->timestamps();

            $table->foreign('idElementoConf')
                ->references('idElementoConf')
                ->on('elemento_configuracion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_ci');
    }
}
