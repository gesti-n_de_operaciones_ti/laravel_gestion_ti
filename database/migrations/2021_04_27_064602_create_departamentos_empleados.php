<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartamentosEmpleados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departamentos_empleados', function (Blueprint $table) {
            $table->integer('idDepartamento')->unsigned();
            $table->bigInteger('idEmpleado')->unsigned();
            $table->timestamps();

            $table->foreign('idDepartamento')
                ->references('idDepartamento')
                ->on('departamentos');

            $table->foreign('idEmpleado')
                ->references('id')
                ->on('empleados');

            $table->unique(['idDepartamento', 'idEmpleado']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departamentos_empleados');
    }
}
