<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
            [ 'idRol' => 1,  'nbRol' => 'Gestor de configuración'],
            [ 'idRol' => 2,  'nbRol' => 'Técnico en configuración'],
            [ 'idRol' => 3,  'nbRol' => 'Solicitador de configuraciones']
        ]);
    }
}
