<?php

namespace Database\Seeders;

use App\Models\Departamentos;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Departamentos::insert([
            ['idDepartamento' => 1, 'nbDepartamento' => 'Biblioteca'],
            ['idDepartamento' => 3, 'nbDepartamento' => 'Bioquímica'],
            ['idDepartamento' => 4, 'nbDepartamento' => 'Cómputo'],
            ['idDepartamento' => 5, 'nbDepartamento' => 'Comunicación'],
            ['idDepartamento' => 6, 'nbDepartamento' => 'Dirección'],
            ['idDepartamento' => 7, 'nbDepartamento' => 'División Acádemica'],
            ['idDepartamento' => 8, 'nbDepartamento' => 'Económico'],
            ['idDepartamento' => 9, 'nbDepartamento' => 'Electrico-Electronica'],
            ['idDepartamento' => 10, 'nbDepartamento' => 'Industrial'],
            ['idDepartamento' => 11, 'nbDepartamento' => 'Financieros']
        ]);

        DB::table('departamentos_empleados')->insert([
            ['idDepartamento'=>4, 'idEmpleado'=>1],
            ['idDepartamento'=>4, 'idEmpleado'=>2],
            ['idDepartamento'=>7, 'idEmpleado'=>3],
        ]);

        DB::table('departamento_encargado')->insert([
            ['idDepartamento'=>4, 'idEncargado'=>1]
        ]);
    }
}
