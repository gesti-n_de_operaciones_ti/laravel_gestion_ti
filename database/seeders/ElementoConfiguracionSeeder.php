<?php

namespace Database\Seeders;

use App\Models\CIDatabase;
use App\Models\CIHardware;
use App\Models\ElementoConfiguracion;
use App\Models\Empleado;
use App\Models\TipoCI;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ElementoConfiguracionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        TipoCI::insertOrIgnore([
            ['siglasTipoCI' => 'HW', 'nbTipoCI'=>'Hardware'],
            ['siglasTipoCI' => 'SW', 'nbTipoCI'=>'Software'],
            ['siglasTipoCI' => 'DB', 'nbTipoCI'=>'Base de datos'],
            ['siglasTipoCI' => 'NET', 'nbTipoCI'=>'Red de comunicaciones']
        ]);

        DB::transaction(function(){
            ElementoConfiguracion::updateOrCreate([
                'idElementoConf' => 1,
                'nbElemento'=> 'Impresora HP Laser 107 W',
                'descripcion' => 'Impresora ubicada en oficina del Centro de Cómputo',
                'siglasTipoCI' => 'HW',
                'idEncargado' => 1,
                'urlImage' => null,
                'idProveedor' => 30,
            ]);

            ElementoConfiguracion::updateOrCreate([
                'idElementoConf' => 2,
                'nbElemento'=> 'Base de datos de financieros',
                'descripcion' => 'Base de datos para gestionar las operaciones del departamento de financieros',
                'siglasTipoCI' => 'DB',
                'idEncargado' => 1,
                'urlImage' => null,
                'idProveedor' => null,
            ]);

            CIHardware::updateOrCreate([
                'idElementoConf' => 1,
                'modelo' => '107 W',
                'ip_address' => '172.16.20.15',
                'idLocalizacion' => 3,
                'numserie' => 'F9FYJ4KDIMPDF'
            ]);

            CIDatabase::updateOrCreate([
                'idElementoConf' => 2,
                'ip_address' => '64.23.232.2',
                'dbms_name' => 'MySQL',
                'version' => '1.2',
            ]);
        });


    }
}
