<?php

namespace Database\Seeders;

use App\Models\Edificio;
use App\Models\Localizacion;
use Illuminate\Database\Seeder;

class EdificiosLocacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Edificio::insert([
            ['idEdificio' => 1, 'clave'=>'A','descUbicacion'=>'Justo al frente de la entrada posterior del tecnológico', 'descripcion'=>'Edificio A'],
            ['idEdificio' => 2, 'clave'=>'B','descUbicacion'=>null, 'descripcion'=>'Edificio B'],
            ['idEdificio' => 3, 'clave'=>'CC', 'descripcion'=>'Centro de cómputo', 'descUbicacion'=>null],
        ]);

        Localizacion::insert([
            [ 'idLocalizacion'=>1, 'claveLocalizacion'=>'EA01', 'idEdificio'=> 1, 'numPlanta'=>1],
            [ 'idLocalizacion'=>2, 'claveLocalizacion'=>'EA02', 'idEdificio'=> 1, 'numPlanta'=>1],
            [ 'idLocalizacion'=>3, 'claveLocalizacion'=>'CCPD', 'idEdificio'=> 3, 'numPlanta'=>2],
            [ 'idLocalizacion'=>4, 'claveLocalizacion'=>'B2', 'idEdificio'=> 2, 'numPlanta'=>2],
        ]);
    }
}
