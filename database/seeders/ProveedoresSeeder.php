<?php

namespace Database\Seeders;

use App\Models\Proveedor;
use Illuminate\Database\Seeder;

class ProveedoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Proveedor::insert([
            [
                'idProveedor' => 30,
                'nbProveedor' => 'HP Inc. México',
                'rfc' => 'CPM15013J82',
                'email' => null,
                'telefono' => '8000-630-234',
                'website' => 'https://support.hp.com/mx-es',
            ],
            [
                'idProveedor' => 31,
                'nbProveedor' => 'OFFICE DEPOT DE MEXICO SA DE CV',
                'rfc' => 'ODM950324V2A',
                'email' => 'sclientes@officedepot.com.mx',
                'telefono' => '6865671550',
                'website' => 'www.officedepot.com.mx',
            ],
        ]);
    }
}
