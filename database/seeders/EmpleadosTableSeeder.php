<?php

namespace Database\Seeders;

use App\Models\Empleado;
use App\Models\RoleEmpleado;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EmpleadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empleado::updateOrCreate([
            'email' => 'jorge_moya@itculiacan.com',
            'password' => Hash::make('password'),
            'name' => 'Jorge',
            'last_name'=>'Moya',
            'username'=>'moya_sistemas',
            'phone'=> '123455678',
            'address'=> 'CALLE 1 Col. Sin nombre',
            'idRol' =>1,
            'id' => 1
        ]);

        Empleado::updateOrCreate([
            'email' => 'tecnico@itculiacan.com',
            'password' => Hash::make('password'),
            'name' => 'Noel',
            'last_name'=>'García Inzunza',
            'username'=>'noel_sistemas',
            'phone'=> '123455678',
            'address'=> 'CALLE 2 Col. Sin nombre',
            'idRol' =>2,
            'id' => 2
        ]);

        Empleado::updateOrCreate([
            'email' => 'general@itculiacan.com',
            'password' => Hash::make('password'),
            'name' => 'Janeth',
            'last_name'=>'Del Rincon',
            'username'=>'janeth_sistemas',
            'phone'=> '123455678',
            'address'=> 'CALLE 3 Col. Sin nombre',
            'idRol' =>3,
            'id' => 3
        ]);

//        RoleEmpleado::create([
//            'idRol' => 1,
//            'idEmpleado' => 1
//        ]);

    }
}
