<?php

namespace Database\Seeders;

use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\Quiz;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SolicitudesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prioridades_solicitudes')->insertOrIgnore([
            ['idPrioridad'=>1, 'nbPrioridad'=>'Baja'],
            ['idPrioridad'=>2, 'nbPrioridad'=>'Media'],
            ['idPrioridad'=>3, 'nbPrioridad'=>'Urgente']
        ]);

        DB::table('estatus_solicitudes')->insertOrIgnore([
            ['idEstatus'=>1, 'nbEstatus'=>'Enviada'],
            ['idEstatus'=>2, 'nbEstatus'=>'En diagnostico'],
            ['idEstatus'=>3, 'nbEstatus'=>'En proceso'],
            ['idEstatus'=>4, 'nbEstatus'=>'En espera al cambio'],
            ['idEstatus'=>5, 'nbEstatus'=>'Procesada'],
            ['idEstatus'=>6, 'nbEstatus'=>'Aprobada'],
            ['idEstatus'=>7, 'nbEstatus'=>'Cancelada']
        ]);

        DB::table('estatus_cambio')->insertOrIgnore([
            ['idEstatusCambio'=>1, 'nbEstatusCambio'=>'Solicitado'],
            ['idEstatusCambio'=>2, 'nbEstatusCambio'=>'Aprobado'],
            ['idEstatusCambio'=>3, 'nbEstatusCambio'=>'Rechazado'],
        ]);


        DB::transaction(function(){
            $quizEvaluacion = Quiz::updateOrCreate(
                [
                    'idQuiz' => 1,
                    'nbQuiz' => 'Evaluación de la incidencia',
                    'deQuiz' => 'Criterios a evaluar del servicio de resolución de incidencias'
                ]
            );

            Question::updateOrCreate(
                [
                    'idQuiz' => 1,
                    'idQuestion' => 1,
                    'key' => 'conformidad',
                    'label' => '¿Cuál es su nivel de conformidad respecto al servicio que se le brindó en general?',
                    'controlType' => 'dropdown',
                    'required' => true,
                    'order' => 1
                ]
            );

            Question::updateOrCreate(
                [
                    'idQuiz' => 1,
                    'idQuestion' => 2,
                    'key' => 'rapidez',
                    'label' => 'Seleccione la velocidad con la que considera que respondieron a su solicitud',
                    'controlType' => 'dropdown',
                    'required' => true,
                    'order' => 2
                ]
            );

            DB::table('question_options')->insertOrIgnore([
                ['idQuestion'=>1, 'key' => 'insatisfecho', 'value' =>  'Insatisfecho'],
                ['idQuestion'=>1, 'key' => 'satisfecho', 'value' =>  'Satisfecho'],
                ['idQuestion'=>1, 'key' => 'satisfecho', 'value' =>  'Muy satisfecho'],
            ]);

            DB::table('question_options')->insertOrIgnore([
                ['idQuestion'=>2, 'key' => 'baja', 'value' =>  'Baja'],
                ['idQuestion'=>2, 'key' => 'media', 'value' =>  'Media'],
                ['idQuestion'=>2, 'key' => 'alta', 'value' =>  'Alta'],
            ]);
        });

    }
}
