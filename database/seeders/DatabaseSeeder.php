<?php

namespace Database\Seeders;

use App\Models\ElementoConfiguracion;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(EmpleadosTableSeeder::class);
        $this->call(DepartmentsSeeder::class);
        $this->call(EdificiosLocacionesSeeder::class);
        $this->call(ProveedoresSeeder::class);
        $this->call(ElementoConfiguracionSeeder::class);
        $this->call(SolicitudesSeeder::class);
    }
}
