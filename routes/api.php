<?php

use App\Http\Controllers\CambiosSolicitudesController;
use App\Http\Controllers\CatalogoServiciosController;
use App\Http\Controllers\EvaluacionesIncidenciasController;
use App\Http\Controllers\LocalizacionesController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\ElementoConfiguracionController;
use App\Http\Controllers\ProveedoresController;
use App\Http\Controllers\IncidenciasController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DepartamentosController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
    Auth
*/
Auth::routes();
Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('session/user', [AuthController::class, 'sessionUser']);
    Route::get('payload', [AuthController::class, 'payload']);
});

Route::get('employees/roles', [EmployeesController::class,'rolesUser']);
Route::get('proveedores', [ProveedoresController::class,'index']);

Route::resource('employees', 'EmployeesController')->except([
    'create','edit'
]);

Route::get('tipos_ci', [ElementoConfiguracionController::class,'tiposCI']);

Route::resource('elementos-configuracion', 'ElementoConfiguracionController')->except([
    'create','edit'
]);

// Localizaciones y Edificios
Route::resource('localizaciones/edificios', 'EdificiosController');
Route::resource('localizaciones', 'LocalizacionesController');

//Departamentos
Route::resource('departments', 'DepartamentosController');

// Solicitudes
Route::get('solicitudes', [IncidenciasController::class,'index']);
Route::get('solicitudes/prioridades', [IncidenciasController::class,'prioridadesSolicitudes']);
Route::get('solicitudes/quiz-evaluacion', [EvaluacionesIncidenciasController::class,'getQuizEvaluacion']);
Route::get('solicitudes/{idSolicitud}', [IncidenciasController::class,'show']);

Route::post('solicitudes', [IncidenciasController::class,'create']);
Route::put('solicitudes/{idSolicitud}/asignar', [IncidenciasController::class,'asignarSolicitud']);
Route::put('solicitudes/{idSolicitud}/diagnosticar', [IncidenciasController::class,'diagnosticarSolicitud']);
Route::put('solicitudes/{idSolicitud}/procesar', [IncidenciasController::class,'procesarSolicitud']);
Route::put('solicitudes/{idSolicitud}/evaluar', [IncidenciasController::class,'evaluarSolicitud']);
Route::get('solicitudes/{idSolicitud}/historial', [IncidenciasController::class,'historialSolicitud']);
Route::get('solicitudes/{idSolicitud}/evaluacion', [EvaluacionesIncidenciasController::class,'evaluacionSolicitud']);

// Catalogo de servicios
Route::get('catalogo-servicios', [CatalogoServiciosController::class,'index']);
Route::post('catalogo-servicios', [CatalogoServiciosController::class,'store']);
Route::delete('catalogo-servicios', [CatalogoServiciosController::class,'destroy']);

// Solicitudes de cambios
Route::get('cambios-solicitudes', [CambiosSolicitudesController::class,'index']);
Route::get('cambios-solicitudes/{idSolicitud}', [CambiosSolicitudesController::class,'show']);
Route::put('cambios-solicitudes/{idSolicitud}/approve', [CambiosSolicitudesController::class,'approve']);
Route::put('cambios-solicitudes/{idSolicitud}/deny', [CambiosSolicitudesController::class,'deny']);